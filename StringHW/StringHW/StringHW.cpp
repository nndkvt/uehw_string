﻿#include <iostream>
#include <string>
#include <iomanip>

int main()
{
    std::string test;

    std::cout << "Enter the string: ";
    std::getline(std::cin, test);
    std::cout << "\n";

    std::cout << "Line's length: " << test.length() << "\n";
    std::cout << "Line's first symbol: " << test[0] << "\n";
    std::cout << "Line's last symbol: " << test[test.length() - 1] << "\n";

    return 0;
}
